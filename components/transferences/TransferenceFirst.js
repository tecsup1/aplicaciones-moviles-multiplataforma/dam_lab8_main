var moment = require('moment');
moment.locale('es');

import React, {Component, useState} from 'react';
import {
  Text,
  View,
  Button,
  TextInput,
  StyleSheet,
  Platform,
  Switch,
  Alert,
} from 'react-native';

import ModalSelector from 'react-native-modal-selector';
import DateTimePicker from '@react-native-community/datetimepicker';
import {TouchableOpacity} from 'react-native-gesture-handler';

import Moment from 'moment';

import MaterialMenu from '../MaterialMenu';


export default class TransferenceFirst extends Component {
  constructor(props) {
    super(props);
    this.state = {
      importe: 0,
      date: new Date(1598051730000),
      //mode: 'date',
      show: false,

      cuentaDeOrigen: 0,
      cuentaDestino: 0,
      importe: 0,
      referencia: 0,
      switchIsEnabled: false,
    };
  }

  onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    this.setState({show: Platform.OS === 'ios'});
    this.setState({date: currentDate});
  };

  showMode() {
    this.setState({show: true});
  }

  toggleSwitch = () => {
    this.setState(prevState => ({switchIsEnabled: !prevState.switchIsEnabled}));
  };

  render() {
    let index = 0;
    const data = [
      {key: index++, section: true, label: 'Cuentas'},
      {key: index++, label: '0000000000231454'},
      {key: index++, label: '0000000000231455'},
      {
        key: index++,
        label: '0000000000231456',
        accessibilityLabel: 'Tap here for cranberries',
      },
    ];

    return (
      <View style={{alignItems: 'center', padding: 15}}>
        <View style={{width: '80%'}}>
          <Text style={{marginTop: 5, fontSize: 18}}>Cuenta de origen</Text>

          <ModalSelector
            data={data}
            onChange={option => {
              //alert(`${option.label} (${option.key}) nom nom nom`);
              this.setState({cuentaDeOrigen: option.label});
            }}
            value={this.state.cuentaDeOrigen}>
            <TextInput
              style={{
                borderWidth: 1,
                borderColor: '#ccc',
                padding: 10,
                height: 45,
                borderRadius: 8,
                textAlign: 'center',
                fontSize: 17,
              }}
              editable={false}
              placeholder="Seleccione cuenta"
              value={this.state.cuentaDeOrigen}
            />
          </ModalSelector>

          <Text style={{marginTop: 5, fontSize: 18}}>Cuenta destino</Text>

          <ModalSelector
            data={data}
            onChange={option => {
              //alert(`${option.label} (${option.key}) nom nom nom`);
              this.setState({cuentaDestino: option.label});
            }}>
            <TextInput
              style={{
                borderWidth: 1,
                borderColor: '#ccc',
                padding: 10,
                height: 45,
                borderRadius: 8,
                textAlign: 'center',
                fontSize: 17,
              }}
              editable={false}
              placeholder="Seleccione cuenta"
              value={this.state.cuentaDestino}
            />
          </ModalSelector>

          <Text style={{marginTop: 5, fontSize: 18}}>Importe</Text>
          <TextInput
            style={myStyles.textInput}
            value={this.state.importe.toString()}
            onChangeText={text => {
              if (!isNaN(text)) this.setState({importe: Number(text)});
            }}
          />

          <Text style={{marginTop: 5, fontSize: 18}}>Referencia</Text>
          <TextInput
            style={myStyles.textInput}
            value={this.state.referencia.toString()}
            onChangeText={text => {
              if (!isNaN(text)) this.setState({referencia: Number(text)});
            }}
          />

          <TouchableOpacity
            onPress={() => this.showMode('date')}
            style={{
              width: '40%',
              backgroundColor: 'red',
              marginTop: 15,
              alignSelf: 'center',
            }}>
            <Text
              style={[
                myStyles.textInput,
                {
                  width: '100%',
                  alignSelf: 'center',
                  paddingTop: '8%',
                },
              ]}>
              {Moment(this.state.date).format('DD MMM YYYY')}
            </Text>
          </TouchableOpacity>
          <View>
            {this.state.show && (
              <DateTimePicker
                testID="dateTimePicker"
                timeZoneOffsetInMinutes={0}
                value={this.state.date}
                mode={'date'}
                is24Hour={true}
                display="default"
                onChange={this.onChange}
              />
            )}
          </View>

          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              margin: 15,
            }}>
            <Text style={{fontSize: 18}}>Notificarme al email</Text>
            <Switch
              trackColor={{false: '#767577', true: '#81b0ff'}}
              thumbColor={this.state.switchIsEnabled ? '#f5dd4b' : '#f4f3f4'}
              ios_backgroundColor="#3e3e3e"
              onValueChange={this.toggleSwitch}
              value={this.state.switchIsEnabled}

              //style={{alignSelf: 'center'}}
            />
          </View>

          <View style={{width: '50%', alignSelf: 'center', marginVertical: 20}}>
            <Button
              title="SIGUIENTE"
              onPress={() => {

                const {
                  cuentaDeOrigen,
                  cuentaDestino,
                  importe,
                  referencia,
                  date,
                  switchIsEnabled,
                } = this.state;

                if (
                  cuentaDeOrigen != 0 &&
                  cuentaDestino != 0 &&
                  importe != 0 &&
                  referencia != 0
                ) {
                  this.props.navigation.navigate('Second', {
                    cuentaDeOrigen: cuentaDeOrigen,
                    cuentaDestino: cuentaDestino,
                    importe: importe,
                    referencia: referencia,
                    date: date,
                    switchIsEnabled: switchIsEnabled,
                  });
                }else{
                  Alert.alert("Ingrese los datos restantes por favor.")
                }
              }}
            />
          </View>
        </View>
        
      </View>
    );
  }
}

const myStyles = StyleSheet.create({
  textInput: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 5,
    backgroundColor: '#d1d1d1',
    textAlign: 'center',
  },
});
